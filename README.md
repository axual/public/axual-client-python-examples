# Axual Python Client Examples

This example repository shows a typical use of the Axual Python Client.
All examples use the same use case and resources, making it easier to compare them

## EOL notice
Please note that the clients will be declared EOL by the end of 2024. Read more about this in the following blog: https://axual.com/update-on-support-of-axual-client-libraries

>>>
**IMPORTANT**
In [axual-client-python `1.0.0`](https://gitlab.com/axual-public/axual-client-python/-/tree/1.0.0), the interface has changed to align with [Confluent client](https://docs.confluent.io/clients-confluent-kafka-python/current/overview.html) (check gitlab [Issue #18](https://gitlab.com/axual-public/axual-client-python/-/issues/18)). This means you need to update your client applications to use `1.0.0`

Refer to axual-client-python [changelog.md](https://gitlab.com/axual-public/axual-client-python/-/blob/master/changelog.md#100-2021-07-20) for all notable changes in `1.0.0`. Compare the [1.0.0-alpha4](https://gitlab.com/axual-public/axual-client-python-examples/-/tree/1.0.0-alpha4) and [1.0.0](https://gitlab.com/axual-public/axual-client-python-examples/-/tree/1.0.0) examples to see how you should modify your Python application.
>>>

## How to use the examples

The examples can be executed very easily against a local standalone platform, which is dockerized. 

1. Start the standalone platform locally using `docker-compose up`. 
All the topics that are used in the examples will be created and configured, and produce/consume access is granted to the applications referred to in the examples.

    **Note**: Check the [`application-singlestandalone.yml`](resources/standalone/configs/application-singlestandalone.yml) to see how the standalone is configured.  

2. Pick any of the example you want to try and open the `*.py` to start the app in your IDE.

   **Example**: [`string-producer`](axual/string-producer.py) is a string producer example application


## Which examples can I find here?
The examples are can be found in two directories [axual](axual) and [kafka](kafka).
The prior uses the axual client and the latter uses confluent_kafka directly.
SASL authentication examples can be found under [kafka/sasl-examples](kafka/sasl-examples).
Below you can find each example use-case and a brief description of the approach. 

| Example (click to jump to the directory)                                             | Authentication Method | Description                                                      |
|--------------------------------------------------------------------------------------|-----------------------|------------------------------------------------------------------|
| [(specific) Axual Avro producer example](axual/avro-producer.py)                     | SSL                   | Uses Axual Client to Produce to stream `avro-applicationlog`     |
| [(specific) Axual Avro consumer example](axual/avro-consumer.py)                     | SSL                   | Uses Axual Client to Consume from stream `avro-applicationlog`   |
| [String Axual producer example](axual/string-producer.py)                            | SSL                   | Uses Axual Client to Produce to stream `string-applicationlog`   |
| [String Axual consumer example](axual/string-consumer.py)                            | SSL                   | Uses Axual Client to Consume from stream `string-applicationlog` |
| [(specific) Plain Kafka Avro producer example](kafka/ssl-examples/avro-producer.py)  | SSL                   | Uses Kafka Client to Produce to stream `avro-applicationlog`     |
| [(specific) Plain Kafka Avro consumer example](kafka/ssl-examples/avro-consumer.py)  | SSL                   | Uses Kafka Client to Consume from stream `avro-applicationlog`   |
| [String Plain Kafka String producer example](kafka/ssl-examples/string-producer.py)  | SSL                   | Uses Kafka Client to Produce to stream `string-applicationlog`   |
| [String Plain Kafka String consumer example](kafka/ssl-examples/string-consumer.py)  | SSL                   | Uses Kafka Client to Consume from stream `string-applicationlog` |
| [(specific) Plain Kafka Avro producer example](kafka/sasl-examples/avro-producer.py) | SASL                  | Uses Kafka Client to Produce to stream `avro-applicationlog`     |
| [(specific) Plain Kafka Avro consumer example](kafka/sasl-examples/avro-consumer.py) | SASL                  | Uses Kafka Client to Consume from stream `avro-applicationlog`   |
| [String Kafka String producer example](kafka/sasl-examples/string-producer.py)       | SASL                  | Uses Kafka Client to Produce to stream `string-applicationlog`   |
| [String Kafka String consumer example](kafka/sasl-examples/string-consumer.py)       | SASL                  | Uses Kafka Client to Consume from stream `string-applicationlog` |

## FAQ

### How do I configure the examples for different platforms?

Please refer to the following table:

| Platform                | Tenant                     | Environment            | Endpoint                                                             | Certificate                                               | Private Key                                              | Root CA Cert                                          |
|-------------------------|----------------------------|------------------------|----------------------------------------------------------------------|-----------------------------------------------------------|----------------------------------------------------------|-------------------------------------------------------|
| Standalone (default)    | `axual`                    | `local`                | `http://127.0.0.1:8081`                                              | examples/client-cert/standalone/standalone.cer            | examples/client-cert/standalone/standalone-private.key   | examples/client-cert/standalone/standalone-caroot.cer | 
| SaaS trial <sup>*</sup> | `axual462cefe`<sup>*</sup> | `default` <sup>*</sup> | `https://axual462cefe-0ad8bf94-discoveryapi.axual.cloud`<sup>*</sup> | `[yourcompany]xxxx-application-xxxx.cert.pem`<sup>*</sup> | `[yourcompany]xxxx-application-xxxx.key.pem`<sup>*</sup> | `[yourcompany]xxxx-root-ca.cert.pem` <sup>*</sup>     | 

<sup>*</sup> **Note**: If you want to run the examples against the SaaS trail environment, you will find information in the `README.txt` of care package, which has been sent to you when requesting your trial.
It will look something like:


    Generic client configuration:
        Tenant name: foobarf431
        Environment name: default
        Note: Keep in mind that this is just a default environment the platform is provided with: you can create more
    
    client configuration:
        Discovery URL: https://foobarf431-e674b685-discoveryapi.axual.cloud 

### How to run the examples

Install the latest `axual-client-python`:
```bash
pip install axual-client-python
```
    
You can also build and install the client locally by cloning and installing it using [poetry](https://python-poetry.org/docs/):
```bash
pushd ..   # go to a directory where you want to clone the client
git clone git@gitlab.com:axual-public/axual-client-python.git
cd axual-client-python/
poetry build # install the poetry as per the instructions on the linked page
popd
pip install ../axual-client-python/dist/*.whl
```
    
### What about the schemas used? 

The Avro schemas used in the examples can be found in the [resources](resources/schemas) folder.

## Known Limitations

* The SASL examples are not compatible to run on local standalone platform. You can try to run them on real environment (like: SaaS trial environment) also make sure to replace the username/password in the examples with one generated in self-service UI.

## License

Axual Client Python Examples is licensed under the [Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0.txt).
