# -*- coding: utf-8 -*-
#
# Copyright (C) 2022 Axual B.V.
#
# Licensed under the Apache License, Version 2.0 (the "License")
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import logging
from os.path import dirname, abspath
from time import gmtime

from axualclient.avro import AvroDeserializer
from axualclient.deserializing_consumer import DeserializingConsumer
from confluent_kafka.cimpl import KafkaError

from resources.schemas import Application, ApplicationLogEvent

logger = logging.getLogger('')  # Root logger, to catch all submodule logs
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s.%(msecs)03d|%(levelname)s|%(filename)s| %(message)s',
                              datefmt='%Y-%m-%d %H:%M:%S')
formatter.converter = gmtime  # Log UTC time

if len(logger.handlers) == 0:
    console = logging.StreamHandler()
    console.setFormatter(formatter)
    logger.addHandler(console)


def _full_path_of(path):
    base_dir = dirname(dirname(abspath(__file__)))
    return f'{base_dir}{path}'


def on_error_callback(error: KafkaError):
    logger.error(error)


def on_commit_callback(error: KafkaError, partitions):
    if error is not None:
        logger.error(f'Failed to commit offsets: {error}: {partitions}')
    else:
        logger.info(f'Committed offsets for: {partitions}')


def log_assignment(consumer_obj, partitions):
    logger.info(f'Consumer assignment: {len(partitions)} partitions:')
    for p in partitions:
        logger.info(f'{p.topic} [{p.partition}] @ {p.offset}')
    consumer_obj.assign(partitions)


def dict_to_application(obj, ctx) -> Application:
    """
    Converts object literal(dict) to a Application instance.
    Args:
        obj (dict): Object literal(dict)
        ctx (SerializationContext): Metadata pertaining to the serialization
            operation.
    """
    if obj is None:
        return None

    return Application(name=obj['name'],
                       version=obj['version'],
                       owner=obj['owner'])


def dict_to_application_log_event(obj, ctx) -> ApplicationLogEvent:
    """
    Converts object literal(dict) to a Application instance.
    Args:
        obj (dict): Object literal(dict)
        ctx (SerializationContext): Metadata pertaining to the serialization
            operation.
    """
    if obj is None:
        return None

    return ApplicationLogEvent(timestamp=obj['timestamp'],
                               application=dict_to_application(obj['source'], ctx),
                               context=obj['context'],
                               level=obj['level'],
                               message=obj['message'])


if __name__ == '__main__':
    # Axual Client configuration
    application_id = 'io.axual.example.client.avro.consumer'
    topic = 'avro-applicationlog'

    key_deserializer = AvroDeserializer(
        # Optional parameters
        schema_str=Application.SCHEMA,
        from_dict=dict_to_application
    )

    value_deserializer = AvroDeserializer(
        # Optional parameters
        schema_str=ApplicationLogEvent.SCHEMA,
        from_dict=dict_to_application_log_event,
    )

    configuration = {
        # Axual configuration
        'application_id': application_id,
        'endpoint': 'http://localhost:8081/',
        'tenant': 'axual',
        'environment': 'local',
        # SSL configuration
        'ssl.certificate.location': _full_path_of('/resources/client-cert/standalone/standalone.cer'),
        'ssl.key.location': _full_path_of('/resources/client-cert/standalone/standalone-private.key'),
        'ssl.ca.location': _full_path_of('/resources/client-cert/standalone/standalone-caroot.cer'),
        # Consumer configuration
        'key.deserializer': key_deserializer,
        'value.deserializer': value_deserializer,

        'auto.offset.reset': 'earliest',
        'on_commit': on_commit_callback,
        'error_cb': on_error_callback,
        # 'debug': 'all',
        'logger': logger
    }

    consumer = DeserializingConsumer(configuration)

    logger.info(f'Starting consumer loop, topic: {topic}. ^C to exit.')
    try:
        consumer.subscribe([topic], on_assign=log_assignment)
        while True:
            msg = consumer.poll()

            if msg is None:
                continue

            if msg.error():
                logger.error(f'Error returned by poll: {msg.error()}')
            else:
                logger.info(
                    f'Received message on topic {msg.topic()} partition {msg.partition()} '
                    f'offset {msg.offset()} key: {str(msg.key())} value: {str(msg.value())}'
                )
                consumer.commit()
    except KeyboardInterrupt:
        logger.info('Caught KeyboardInterrupt, stopping.')
    finally:
        if consumer is not None:
            logger.info('Committing final offsets and leaving group.')
            consumer.commit()
            consumer.close()
