# -*- coding: utf-8 -*-
#
# Copyright (C) 2022 Axual B.V.
#
# Licensed under the Apache License, Version 2.0 (the "License")
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import logging
import time
from os.path import dirname, abspath
from time import gmtime

from confluent_kafka import SerializingProducer
from confluent_kafka.schema_registry import SchemaRegistryClient
from confluent_kafka.schema_registry.avro import AvroSerializer

from resources.schemas import Application, ApplicationLogEvent, ApplicationLogLevel

logger = logging.getLogger('')  # Root logger, to catch all submodule logs
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s.%(msecs)03d|%(levelname)s|%(filename)s| %(message)s',
                              datefmt='%Y-%m-%d %H:%M:%S')
formatter.converter = gmtime  # Log UTC time

if len(logger.handlers) == 0:
    console = logging.StreamHandler()
    console.setFormatter(formatter)
    logger.addHandler(console)


def _full_path_of(path):
    base_dir = dirname(dirname(dirname(abspath(__file__))))
    return f'{base_dir}{path}'


def application_to_dict(app: Application, ctx) -> dict:
    """
    Returns a dict representation of a Application instance for serialization.

    Args:
        app (Application): Application instance.
        ctx (SerializationContext): Metadata pertaining to the serialization
            operation.
    Returns:
        dict: Dict populated with application attributes to be serialized.
    """
    return dict(
        name=app.name,
        version=app.version,
        owner=app.owner
    )


def application_log_event_to_dict(application_log_event, ctx) -> dict:
    """
    Returns a dict representation of a ApplicationLogEvent instance for serialization.
    Returns:
        dict: Dict populated with ApplicationLogEvent attributes to be serialized.
    """
    return dict(
        timestamp=application_log_event.timestamp,
        source=application_to_dict(application_log_event.source, ctx),
        context=application_log_event.context,
        level=application_log_event.level,
        message=application_log_event.message
    )


def delivery_callback(error, msg) -> None:
    """
    Per-message delivery callback (triggered by poll() or flush())

    :param error: None if the message was successfully delivered
    :param msg: Message metadata
    :return: None
    """
    if error is not None:
        logger.error(f'Failed to deliver message: {error}')
    else:
        logger.info(f'Produced record to topic {msg.topic()} partition [{msg.partition()}] @ offset {msg.offset()}')


if __name__ == '__main__':
    # Resolved stream name
    # ex : <tenant>-<instance>-<environment>-<streamName>
    # If the topic pattern is different in your case, use that pattern to resolve topic name
    topic = 'demo-local-example-avro-applicationlog'

    # Replace SASL username and password with one generated in Self-Service
    sasl_username = 'username'
    sasl_password = 'password'

    # Schema registry client configuration
    schema_registry_conf = {'url': 'https://platform.local:24000/',
                            'ssl.ca.location': _full_path_of('/resources/client-cert/standalone/standalone-caroot.cer'),
                            'basic.auth.user.info': sasl_username+':'+sasl_password
                            }
    schema_registry_client = SchemaRegistryClient(schema_registry_conf)

    key_serializer = AvroSerializer(
        schema_registry_client,
        Application.SCHEMA,
        application_to_dict
    )

    value_serializer = AvroSerializer(
        schema_registry_client,
        ApplicationLogEvent.SCHEMA,
        application_log_event_to_dict
    )

    # Kafka producer configuration
    configuration = {
        'bootstrap.servers': 'platform.local:9097',
        # Client.ID config
        # An id string to pass to the server when making requests.
        # The purpose of this is to be able to track the source of requests beyond
        # just ip/port by allowing a logical application name to be included in server-side request logging.
        'client.id': 'demo-local-example-io.axual.example.client.avro.producer',
        # security configuration
        'sasl.username': sasl_username,
        'sasl.password': sasl_password,
        'security.protocol': 'SASL_SSL',
        'sasl.mechanism': 'SCRAM-SHA-256',
        'ssl.endpoint.identification.algorithm': 'none',
        # Producer client CA location so client can trust brokers
        'ssl.ca.location': _full_path_of('/resources/client-cert/standalone/standalone-caroot.cer'),
        # key and value serializer
        'key.serializer': key_serializer,
        'value.serializer': value_serializer,
        'acks': 'all',
    }

    producer = SerializingProducer(configuration)

    try:
        logger.info(f'Starting kafka SASL avro producer to produce to topic: {topic}. ^C to exit.')
        n = 0
        while True:
            key = Application(name='value_log_event_producer',
                              version='0.0.1',
                              owner='Axual')
            value = ApplicationLogEvent(timestamp=int(round(time.time() * 1000)),
                                        application=key,
                                        context={'Some key': 'Some Value'},
                                        level=ApplicationLogLevel.INFO,
                                        message=f'Message #{n}')
            producer.poll(0)
            producer.produce(topic=topic, value=value, key=key, on_delivery=delivery_callback)

            time.sleep(1.)
            n += 1
    except KeyboardInterrupt:
        logger.info('Caught KeyboardInterrupt, stopping.')
    finally:
        if producer is not None:
            logger.info('Flushing producer.')
            producer.flush()
