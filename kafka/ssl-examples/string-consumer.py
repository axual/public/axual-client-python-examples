# -*- coding: utf-8 -*-
#
# Copyright (C) 2022 Axual B.V.
#
# Licensed under the Apache License, Version 2.0 (the "License")
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import logging
from os.path import dirname, abspath
from time import gmtime

from confluent_kafka import Consumer
from confluent_kafka.cimpl import KafkaError

logger = logging.getLogger('')  # Root logger, to catch all submodule logs
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s.%(msecs)03d|%(levelname)s|%(filename)s| %(message)s',
                              datefmt='%Y-%m-%d %H:%M:%S')
formatter.converter = gmtime  # Log UTC time

if len(logger.handlers) == 0:
    console = logging.StreamHandler()
    console.setFormatter(formatter)
    logger.addHandler(console)


def _full_path_of(path):
    base_dir = dirname(dirname(dirname(abspath(__file__))))
    return f'{base_dir}{path}'


def on_error_callback(error: KafkaError):
    logger.error(error)


def on_commit_callback(error: KafkaError, partitions):
    if error is not None:
        logger.error(f'Failed to commit offsets: {error}: {partitions}')
    else:
        logger.info(f'Committed offsets for: {partitions}')


def log_assignment(consumer_obj, partitions):
    logger.info(f'Consumer assignment: {len(partitions)} partitions:')
    for p in partitions:
        logger.info(f'\t{p.topic} [{p.partition}] @ {p.offset}')
    consumer_obj.assign(partitions)


if __name__ == '__main__':
    # Resolved stream name
    # ex : <tenant>-<instance>-<environment>-<streamName>
    # If the topic pattern is different in your case, use that pattern to resolve topic name
    topic = 'axual-example-local-string-applicationlog'

    # consumer configuration
    configuration = {
        'bootstrap.servers': 'localhost:8084',
        # Resolved groupId
        # ex: <tenant>-<instance>-<environment>-<applicationId>
        # If the 'group.id' pattern in different in your case, use that pattern to resolve group.id
        'group.id': 'axual-example-local-io.axual.example.client.string.consumer',
        # Client.ID config
        # An id string to pass to the server when making requests.
        # The purpose of this is to be able to track the source of requests beyond
        # just ip/port by allowing a logical application name to be included in server-side request logging.
        'client.id': 'axual-example-local-io.axual.example.client.string.consumer',
        # SSL configuration
        'security.protocol': 'SSL',
        'ssl.endpoint.identification.algorithm': 'none',
        'ssl.certificate.location': _full_path_of('/resources/client-cert/standalone/standalone.cer'),
        'ssl.key.location': _full_path_of('/resources/client-cert/standalone/standalone-private.key'),
        'ssl.ca.location': _full_path_of('/resources/client-cert/standalone/standalone-caroot.cer'),

        'auto.offset.reset': 'earliest',
        'on_commit': on_commit_callback,
        'error_cb': on_error_callback,
        # 'debug': 'all',
        'logger': logger
    }

    consumer = Consumer(configuration)

    logger.info(f'Starting kafka consumer loop, topic: {topic}. ^C to exit.')
    try:
        consumer.subscribe([topic], on_assign=log_assignment)
        while True:
            msg = consumer.poll(1.0)
            if msg is None:
                continue

            if msg.error():
                logger.error(f'Error returned by poll: {msg.error()}')
            else:
                logger.info(
                    f'Received message on topic {msg.topic()} partition {msg.partition()} '
                    f'offset {msg.offset()} key {msg.key()} value {msg.value()}'
                )
                consumer.commit()
    except KeyboardInterrupt:
        logger.info('Caught KeyboardInterrupt, stopping.')
    finally:
        if consumer is not None:
            logger.info('Committing final offsets and leaving group.')
            consumer.commit()
            consumer.close()
