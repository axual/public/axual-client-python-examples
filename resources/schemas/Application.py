# -*- coding: utf-8 -*-
#
# Copyright (C) 2020 Axual B.V.
#
# Licensed under the Apache License, Version 2.0 (the "License")
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


class Application:
    """
    Application record
    Args:
        name (str): The name of the application
        version (str): (Optional) The application version
        owner (str): The owner of the application
    """

    SCHEMA = """
    {
        "namespace":"io.axual.client.example.schema",
        "doc":"Identification of an application",
        "name":"Application","type":"record",
        "fields": [
            {
                "doc":"The name of the application",
                "name":"name","type":"string"
            },
            {
                "doc":"(Optional) The application version",
                "name":
                "version",
                "type":["null","string"],
                "default":null
            },
            {
                "doc":"The owner of the application",
                "name":"owner",
                "type":["null","string"],
                "default":null
            }
        ]
    }"""

    def __init__(self, name: str, version: str, owner: str):
        self.name = name
        self.version = version
        self.owner = owner

    def __str__(self):
        return f'name:{self.name}, version:{self.version}, owner:{self.owner}'
