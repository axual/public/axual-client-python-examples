# -*- coding: utf-8 -*-
#
# Copyright (C) 2020 Axual B.V.
#
# Licensed under the Apache License, Version 2.0 (the "License")
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

from .Application import Application
from .ApplicationLogLevel import ApplicationLogLevel


class ApplicationLogEvent:
    """
    ApplicationLogEvent record
    Args:
        timestamp (int): Timestamp of the event
        application (Application): source The application that sent the event
        context (dict): context The application context, contains application-specific key-value pairs
        level (ApplicationLogLevel): level The log level, being either DEBUG, INFO, WARN or ERROR
        message (str): message The log message
    """

    SCHEMA = """
    {
        "type":"record",
        "name":"ApplicationLogEvent",
        "namespace":"io.axual.client.example.schema",
        "doc":"Generic application log event",
        "fields":[
            {
                "name":"timestamp",
                "type":"long",
                "doc":"Timestamp of the event"
            },
            {
                "name":"source",
                "type":
                    {
                        "type":"record",
                        "name":"Application",
                        "doc":"Identification of an application",
                        "fields":[
                            {
                                "name":"name",
                                "type":"string",
                                "doc":"The name of the application"
                            },
                            {
                                "name":"version",
                                "type":["null","string"],
                                "doc":"(Optional) The application version",
                                "default":null
                            },
                            {
                                "name":"owner",
                                "type":["null","string"],
                                "doc":"The owner of the application","default":null
                            }
                        ]
                    },
                "doc":"The application that sent the event"
            },
            {
                "name":"context",
                "type":
                    {
                        "type":"map",
                        "values":"string"
                    },
                "doc":"The application context, contains application-specific key-value pairs"
            },
            {
                "name":"level",
                "type":
                    {
                        "type":"enum","name":"ApplicationLogLevel",
                        "doc":"The level of the log message",
                        "symbols":["DEBUG","INFO","WARN","ERROR","FATAL"]
                    },
                "doc":"The log level, being either DEBUG, INFO, WARN or ERROR"},
            {
                "name":"message",
                "type":"string",
                "doc":"The log message"
            }
        ]
    }"""

    def __init__(self,
                 timestamp: int,
                 application: Application,
                 context: dict,
                 level: ApplicationLogLevel,
                 message: str):
        self.timestamp = timestamp
        self.source = application
        self.context = context
        self.level = level
        self.message = message

    def __str__(self):
        return f'timestamp:{self.timestamp}, source:{self.source}, context:{self.context}' \
               f', level:{self.level}, message:{self.message}'
